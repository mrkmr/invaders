SRC = src
OBJ = obj
MKDIR = mkdir -p
DEPS = ${SRC}/main.h
SRC_FILES = $(wildcard ${SRC}/*.c)
OBJ_FILES = $(patsubst ${SRC}/%.c,${OBJ}/%.o,$(SRC_FILES))
BINARY = invaders

CC = gcc
CFLAGS = -I${SRC} -lncursesw

all: dirs ${BINARY}

${OBJ}/%.o: ${SRC}/%.c ${DEPS}
	${CC} -Wall -c -o $@ $< ${CFLAGS}

${BINARY}: ${OBJ_FILES}
	$(CC) -o $@ $^ ${CFLAGS}

dirs:
	${MKDIR} ${OBJ}

run:
	./${BINARY}

clean:
	rm ${OBJ_FILES} ${BINARY}
