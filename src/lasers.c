#include <main.h>

extern struct pos ship, size;

bool if_hit_enemy (struct laser* current_laser, struct enemy* current_enemy);

void fire() {
	for (size_t i = 0; i < sizeof(lasers) / sizeof(struct laser); i++) {
		if (!lasers[i].on_screen) {
			lasers[i].pos.col = ship.col + SHIP_HALF_WIDTH;
			lasers[i].pos.row = ship.row - SHIP_HEIGHT - 1;
			lasers[i].on_screen = true;
			attron(COLOR_PAIR(2));
			mvaddstr(lasers[i].pos.row, lasers[i].pos.col, "▌");
			attroff(COLOR_PAIR(2));
			break;
		}
	}
}

void clear_laser (struct laser* current_laser) {
	mvaddstr(current_laser->pos.row + 1, current_laser->pos.col, " ");
}

bool if_hit_enemies (struct laser* current_laser) {
	struct enemy* current_enemy;
	for (size_t row = 0; row < ENEMY_ROW; row++)
		for (size_t col = 0; col < ENEMY_COL; col++) {
			current_enemy = &enemies[row][col];
			if(if_hit_enemy(current_laser, current_enemy))
				return true;
		}
	return false;
}

bool if_hit_enemy (struct laser* current_laser, struct enemy* current_enemy) {
	if (!current_enemy->dead &&
	    /* check if the laser is below
	     * the top of the invader */
	    current_laser->pos.row >=
	    current_enemy->pos.row &&
	    /* check if the laser is above
	     * the bottom of the invader */
	    current_laser->pos.row <=
	    current_enemy->pos.row + ENEMY_HEIGHT &&
	    /* check if the laser is to the left
	     * of the right boundary of the invader */
	    current_laser->pos.col <=
	    current_enemy->pos.col + ENEMY_WIDTH &&
	    /* check if the laser is to the right
	     * of the left boundary */
	    current_laser->pos.col >=
	    current_enemy->pos.col) {
		/* get rid of laser */
		--current_laser->pos.row;
		clear_laser(current_laser);
		current_laser->on_screen = false;
		/* get rid of enemy */
		current_enemy->dead = true;
		clear_enemy(current_enemy);
		return true;
	}
	return false;
}

void draw_lasers() {
	for (size_t i = 0; i < sizeof(lasers) / sizeof(struct laser); i++) {
		/* +1 so lasers don't get stuck on the top */
		struct laser* current_laser = &lasers[i];
		if (current_laser->on_screen) {
			if (current_laser->pos.row + 1 < 0) {
				current_laser->on_screen = false;
			} else if (if_hit_enemies(current_laser)) {
				continue;
			} else {
				--current_laser->pos.row;
				attron(COLOR_PAIR(2));
				mvaddstr(current_laser->pos.row,
					 current_laser->pos.col, "▌");
				attroff(COLOR_PAIR(2));
				clear_laser(current_laser);
			}
		}
	}
}
