#include <main.h>
#include <locale.h>
#include <unistd.h>

struct pos ship, size, center;
int block_div;

bool cont = true;                        /* main loop variable: 'continue' */
bool ship_move = true;                   /* did the ship move?             */

void draw_blocks() {
	mvaddstr(size.row - 8, block_div, "▄████████▄");
	mvaddstr(size.row - 7, block_div, "██████████");
	mvaddstr(size.row - 6, block_div, "███▀  ▀███");
}

void input(int key)
{
	switch(key) {
		case 'q': /* quit! */
			cont = false;
			break;
		case 'h': /* move left */
			move_ship(-SHIP_MOVE_DISTANCE);
			ship_move = true;
			break;
		case 'l': /* move right */
			move_ship(SHIP_MOVE_DISTANCE);
			ship_move = true;
			break;
		case 'f': /* pew pew */
			fire();
			break;
	}
}

void draw()
{
	input(getch());
	if (ship_move) draw_ship();
	draw_lasers();
	draw_enemies();
	refresh();
	usleep(SLEEP_TIME * 1000);
}

void menu()
{
	struct pos menu_center;
	menu_center.row = MENU_HEIGHT / 2;
	menu_center.col = MENU_WIDTH / 2;

	WINDOW* menu = newwin(MENU_HEIGHT, MENU_WIDTH, center.row -
			      menu_center.row, center.col - menu_center.col);

	wattron(menu, A_UNDERLINE);
	mvwaddstr(menu, 2, menu_center.col - 4, "CONTROLS");
	wattroff(menu, A_UNDERLINE);

	mvwaddstr(menu, 5, menu_center.col - 5, "h: move left");
	mvwaddstr(menu, 6, menu_center.col - 5, "l: move right");
	mvwaddstr(menu, 7, menu_center.col - 5, "f: fire");
	mvwaddstr(menu, 8, menu_center.col - 5, "q: quit");
	mvwaddstr(menu, MENU_HEIGHT - 3, (MENU_WIDTH / 2) - 12,
		  "press any key to continue");
	box(menu, 0, 0);
	refresh();
	wrefresh(menu);
	getch();
	clear();
}

int main()
{
	setlocale(LC_ALL, "");                 /* For unicode wide-char       */
	initscr();
	raw();                                 /* no <C-c>                    */
	noecho();                              /* don't echo characters back  */
	curs_set(0);                           /* hide cursor                 */
	getmaxyx(stdscr, size.row, size.col);  /* get terminal dimensions     */
	center.row = size.row / 2;
	center.col = size.col / 2;

	start_color();
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_YELLOW, COLOR_BLACK);
	/* initialize the ship's starting position */
	ship.row = size.row;
	ship.col = center.col - 6;

	menu();
	nodelay(stdscr, TRUE);             /* don't wait for getch            */

	draw_ship();
	initialize_enemies();
	refresh();
	while (cont) draw();

	endwin();
	return 0;
}
