#include <ncurses.h>

#define SLEEP_TIME 40
#define MENU_WIDTH 50
#define MENU_HEIGHT 25

#define SHIP_MOVE_DISTANCE 2
#define SHIP_WIDTH 11
#define SHIP_HALF_WIDTH SHIP_WIDTH / 2
#define SHIP_HEIGHT 3

#define ENEMY_COL 4
#define ENEMY_ROW 2
#define ENEMY_WIDTH 12
#define ENEMY_HALF_WIDTH ENEMY_WIDTH / 2
#define ENEMY_HEIGHT 4

#define MAX_LASERS 6

struct pos {
	int row;
	int col;
};

struct laser {
	struct pos pos;
	bool on_screen;
};

struct enemy {
	struct pos pos;
	bool dead;
	char type;        /* either (c)urly, (m)oe, (l)arry */
};


struct laser lasers[MAX_LASERS - 1];
struct enemy enemies[ENEMY_COL - 1][ENEMY_ROW - 1];


void clear_enemy(struct enemy* current_enemy);
void draw_enemies();
void draw_lasers();
void draw_ship();
void fire();
void initialize_enemies();
void move_ship(int direction);
