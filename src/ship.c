#include <main.h>

extern struct pos ship, size;
extern bool ship_move;

void move_ship (int move_vector) {
	    /* left bound */
	if ((ship.col <= SHIP_HALF_WIDTH && move_vector > 0) ||
	    /* right bound */
	    (ship.col + 1.5 * SHIP_WIDTH > size.col && move_vector < 0) ||
	    /* in the middle */
	    (ship.col - SHIP_HALF_WIDTH > 0 &&
	         ship.col + 1.5 * SHIP_WIDTH < size.col))
		/* actually move */
		ship.col = ship.col + move_vector;
}

/* remove the ghost */
void clear_ship()
{
	move(ship.row - 3, 0);
	clrtoeol();
	move(ship.row - 2, 0);
	clrtoeol();
	move(ship.row - 1, 0);
	clrtoeol();
}

void draw_ship()
{
	clear_ship();
	attron(COLOR_PAIR(1));
	mvaddstr(ship.row - 3, ship.col, "    ▄█▄    ");
	mvaddstr(ship.row - 2, ship.col, "▄█████████▄");
	mvaddstr(ship.row - 1, ship.col, "▀▀▀▀▀▀▀▀▀▀▀");
	attroff(COLOR_PAIR(1));
	ship_move = false;
}
