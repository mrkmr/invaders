#include <main.h>
#include <stdlib.h>

extern struct pos ship, size;

bool enemies_move_left = false;

long random_range(long max);

void draw_curly(struct pos* position) {
	mvaddstr(position->row + 0, position->col, "    ▄██▄    ");
	mvaddstr(position->row + 1, position->col, "  ▄█▀██▀█▄  ");
	mvaddstr(position->row + 2, position->col, "  ▀▀█▀▀█▀▀  ");
	mvaddstr(position->row + 3, position->col, "  ▄▀▄▀▀▄▀▄  ");
}

void draw_moe(struct pos* position) {
	mvaddstr(position->row + 0, position->col, "   ▀▄   ▄▀  ");
	mvaddstr(position->row + 1, position->col, "  ▄█▀███▀█▄ ");
	mvaddstr(position->row + 2, position->col, " █▀███████▀█");
	mvaddstr(position->row + 3, position->col, " ▀ ▀▄▄ ▄▄▀ ▀");
}

void draw_larry(struct pos* position) {
	mvaddstr(position->row + 0, position->col, " ▄▄▄████▄▄▄ ");
	mvaddstr(position->row + 1, position->col, "███▀▀██▀▀███");
	mvaddstr(position->row + 2, position->col, "▀▀███▀▀███▀▀");
	mvaddstr(position->row + 3, position->col, " ▀█▄ ▀▀ ▄█▀ ");
}

void switch_init_enemy(size_t row, size_t div) {
	enemies[row][div].pos.row = 1 + (row * (ENEMY_HEIGHT + 1));
	enemies[row][div].pos.col = (div * (1.5 * ENEMY_WIDTH));
	enemies[row][div].dead = false;

	switch(random_range(2)) {
		case 0:
			enemies[row][div].type = 'c';
			draw_curly(&enemies[row][div].pos);
			break;
		case 1:
			enemies[row][div].type = 'm';
			draw_moe(&enemies[row][div].pos);
			break;
		case 2:
			enemies[row][div].type = 'l';
			draw_larry(&enemies[row][div].pos);
			break;
	}
}

void initialize_enemies() {
	for (size_t row = 0; row < ENEMY_ROW; row++)
		for (size_t col = 0; col < ENEMY_COL; col++)
			switch_init_enemy(row, col);
}

void check_bounds() {
	/* don't go off the left side! */
	if (enemies[0][0].pos.col <= 0)
		enemies_move_left = false;
	/* don't go off the right side! */
	else if (enemies[ENEMY_ROW - 1][ENEMY_COL - 1].pos.col + ENEMY_WIDTH >=
		 size.col)
		enemies_move_left = true;
}

void clear_enemy(struct enemy* current_enemy) {
	for (size_t y = 0; y < ENEMY_HEIGHT; y++)
		for (size_t i = 0; i < ENEMY_WIDTH; i++)
			mvaddstr(current_enemy->pos.row + y,
				 current_enemy->pos.col + i, " ");
}

void move_enemy_horizontal(size_t row, size_t col) {
	struct enemy* current_enemy = &enemies[row][col];
	clear_enemy(current_enemy);
	/* what way should the invaders move? */
	/*enemies_move_left ? --current_enemy->pos.col : ++current_enemy->pos.col;*/
	switch (current_enemy->type) {
		case 'c':
			draw_curly(&current_enemy->pos);
			break;
		case 'm':
			draw_moe(&current_enemy->pos);
			break;
		case 'l':
			draw_larry(&current_enemy->pos);
			break;
	}
}

void draw_enemies() {
	check_bounds();
	for (size_t row = 0; row < ENEMY_ROW; row++)
		for (size_t col = 0; col < ENEMY_COL; col++)
			if (!enemies[row][col].dead)
				move_enemy_horizontal(row, col);
}

/* from http://stackoverflow.com/questions/2509679/
 * for range of random numbers */
long random_range (long max) {
	unsigned long
		num_bins = (unsigned long) max + 1,
		num_rand = (unsigned long) RAND_MAX + 1,
		bin_size = num_rand / num_bins,
		defect   = num_rand % num_bins;
	long x;

	do {
		x = random();
	} while (num_rand - defect <= (unsigned long)x);

	return x / bin_size;
}
